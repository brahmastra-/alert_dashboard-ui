import { Component, Inject, OnInit } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { getStyle, rgbToHex } from '@coreui/coreui/dist/js/coreui-utilities';
import { BackendApiService } from '../../services/service-api.service';
import { Router } from '@angular/router';

@Component({
  templateUrl: 'colors.component.html'
})
export class ColorsComponent implements OnInit {
private serviceData;


  constructor(
    @Inject(DOCUMENT) private _document: any,
    private backendApiService: BackendApiService,
    private router: Router
  ) {}

  public themeColors(): void {
    Array.from(this._document.querySelectorAll('.theme-color')).forEach((el: HTMLElement) => {
      const background = getStyle('background-color', el);
      const table = this._document.createElement('table');
      table.innerHTML = `
        <table class="w-100">
          <tr>
            <td class="text-muted">HEX:</td>
            <td class="font-weight-bold">${rgbToHex(background)}</td>
          </tr>
          <tr>
            <td class="text-muted">RGB:</td>
            <td class="font-weight-bold">${background}</td>
          </tr>
        </table>
      `;
      el.parentNode.appendChild(table);
    });
  }

  ngOnInit(): void {
    this.themeColors();

    // this.backendApiService.getAllServices().subscribe((data)=>{
    //   console.log("calling get all service api....");
    //   this.serviceData = data['articles'];
    // });

    
  }

  onAddService(){

    console.log("service add......");
    this.router.navigate(['/theme/typography']);
  }
}
