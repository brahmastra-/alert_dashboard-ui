import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { BackendApiService } from '../../services/service-api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {
  
  // loginForm: FormGroup;

  loginForm = new FormGroup({
    Username: new FormControl(''),
    Password: new FormControl('')
    })


  constructor( 
    private fb: FormBuilder,
    private backendApiService:BackendApiService,
    private router: Router,
    ) 
    {
      this.eslLoginForm();
     }
     eslLoginForm() {
      this.loginForm = this.fb.group({
        Username: [''],
        Password: ['']
      });
    }

  ngOnInit(): void {

  }

  onLoginSubmit(){
    console.log('Your form data : ', this.loginForm.value );

    // console.log('Your form data : ', this.loginForm.User );


    // if(){

    // }

    this.router.navigate(['/dashboard']);
    // this.router.navigate(['/theme/colors']);
   
  }
  
}