import { TestBed } from '@angular/core/testing';

import { BackendApiService } from './service-api.service';

describe('ServiceApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BackendApiService = TestBed.get(BackendApiService);
    expect(service).toBeTruthy();
  });
});
