// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  envName: 'dev',
  REST_API_URL: "http://52.66.150.242",
  //REST_API_URL: "http://13.232.197.25",

    BASE_URL_CDN: "http://dchjc0e2km5t.cloudfront.net",
    messages:{ "COMMON_PAGE_ACCESS_DENIED":"You do not have access to this page",
    "CALL_PAGE_ACCESS_DENIED":"Call page is not accessible to operation user",
    "DISCLAIMER_REPORT":" Success Rate is calculated excluding Open Calls."
      },
      config:{
      "logoURLPath":"https://s3.ap-south-1.amazonaws.com/bts-vas-dev/tenant/images/"
    }
};
